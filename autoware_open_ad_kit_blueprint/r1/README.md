# Autoware Open AD Kit Blueprint R1
## Overview
In this first release of the Autoware Open AD Kit blueprint three containers will be used
* Open AD Kit v1.0
    * This is the containerized workload application
* Scenario Simulator
* RViz

which will show, in RViz, how a vehicle nagvigates a predefined route set in Scenario Simulator.

### Open AD Kit v1.0
The application workload container is the one used in [Open AD Kit v1.0](https://gitlab.com/autowarefoundation/autoware_reference_design/-/tree/main/docs/Appendix/Open-AD-Kit-Start-Guide) from Autoware Foundation. Autoware is deployed as a monolithic container and runs the following features from the Autoware.Auto software stack:
* map
* perception
* planning
* vehicle interface

<img src="./images/oak_bp_r1_autoware.jpg" alt="oak_bp_r1_autoware" width="600"/>

These features are implemented using ROS 2 and uses CycloneDDS as the data transportation layer. The container which runs the  workload is based on Ubuntu 20.04 LTS. The workload is exercised using Scenario Simulator and visualized using RViz.

### Scenario Simulator
The scenario simulator determines the route which the vehicle should follow.

<img src="./images/oak_bp_r1_scen_sim.jpg" alt="oak_bp_r1_scen_sim" width="600"/>

### RViz
ROS Visualizer (RViz) is a 3D visualization tool for ROS and allows a user to see the simulated vehicle following a path determined by the scenario simulator.

<img src="./images/oak_bp_r1_visualization.jpg" alt="oak_bp_r1_visualization" width="600"/>

## Versions
| Release Date | Version | OS | Hardware |
| ------------ | ------- | ----- | -------- |
| 2022 October | [v1.0](oak_bp_v1_0.md) | EWAOL v1.0 (Kirkstone), baremetal | AVA Developer Platform |
| 2022 October | [v1.1](oak_bp_v1_1.md) | EWAOL v1.0 (Kirkstone), baremetal, xfce | AVA Developer Platform |
| 2022 October | [v1.2](oak_bp_v1_2.md) | EWAOL v1.0 (Kirkstone), baremetal-sdk, xfce | AVA Developer Platform + NVIDIA GPU |
